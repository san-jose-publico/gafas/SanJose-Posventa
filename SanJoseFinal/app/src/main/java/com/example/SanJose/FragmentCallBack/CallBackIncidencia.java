package com.example.SanJose.FragmentCallBack;

import com.example.SanJose.Models.Incidencia;

public interface CallBackIncidencia {
    void onIncidenciaSent(Incidencia incidencia);
}
