package com.example.SanJose.Models;

public class NumIncidencia {
    private int numIncidencias;

    public int getNumIncidencias() {
        return numIncidencias;
    }

    public void setNumIncidencias(int numIncidencias) {
        this.numIncidencias = numIncidencias;
    }
}
