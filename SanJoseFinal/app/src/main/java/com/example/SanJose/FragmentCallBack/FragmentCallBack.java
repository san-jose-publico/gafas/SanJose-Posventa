package com.example.SanJose.FragmentCallBack;

import java.io.File;

public interface FragmentCallBack {
    void onDataSent(File yourData);
}
