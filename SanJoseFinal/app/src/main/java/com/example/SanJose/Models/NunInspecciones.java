package com.example.SanJose.Models;

public class NunInspecciones {
    public Integer num;

    public NunInspecciones(Integer num) {
        this.num = num;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }
}
